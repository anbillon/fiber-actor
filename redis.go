// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package actor

import (
	"github.com/go-redis/redis"
	"gitlab.com/anbillon/fiber"
	"gitlab.com/anbillon/slago"
)

// Type definition for redis client which should implement close func.
type RedisClient interface {
	redis.Cmdable
	Close() error
}

type singleClient struct {
	redis.Cmdable
	client *redis.Client
}

type clusterClient struct {
	redis.Cmdable
	client *redis.ClusterClient
}

// RedisProperty defines the property of fiber.redis section in yaml config.
type RedisProperty struct {
	// host:port address
	Address string `fiber:"address"`

	// password for redis if existed
	Password string `fiber:"password"`

	// the db to select after connected
	Db int `fiber:"db"`

	PoolConfig struct {
		Size    int `fiber:"size"`
		MinIdle int `fiber:"min_idle"`
	} `fiber:"pool"`

	// use sentinel mode or not, default false
	SentinelConfig struct {
		Master string   `fiber:"master"`
		Nodes  []string `fiber:"nodes"`
	} `fiber:"sentinel"`

	ClusterConfig struct {
		// A seed list of host:port addresses of cluster unresolvedNodes.
		Nodes        []string `fiber:"nodes"`
		MaxRedirects int      `fiber:"max_redirects"`
	} `fiber:"cluster"`
}

func (*RedisProperty) Prefix() string {
	return "fiber.redis"
}

func init() {
	fiber.Wire(&RedisProperty{}, newRedisClient)
}

// newRedisClient creates a new instance of redis client.
func newRedisClient(property *RedisProperty) RedisClient {
	if len(property.Address) == 0 && len(property.ClusterConfig.Nodes) == 0 {
		slago.Logger().Fatal().Msg("no redis address or cluster nodes found")
	}

	var redisClient RedisClient
	if property.ClusterConfig.Nodes == nil {
		var client *redis.Client
		if property.SentinelConfig.Nodes == nil {
			client = redis.NewClient(&redis.Options{
				Addr:         property.Address,
				Password:     property.Password,
				DB:           property.Db,
				PoolSize:     property.PoolConfig.Size,
				MinIdleConns: property.PoolConfig.MinIdle,
			})
		} else {
			client = redis.NewFailoverClient(&redis.FailoverOptions{
				SentinelAddrs: property.SentinelConfig.Nodes,
				Password:      property.Password,
				DB:            property.Db,
				PoolSize:      property.PoolConfig.Size,
				MinIdleConns:  property.PoolConfig.MinIdle,
			})
		}

		redisClient = &singleClient{
			Cmdable: client,
			client:  client,
		}
	} else {
		client := redis.NewClusterClient(&redis.ClusterOptions{
			Addrs:        property.ClusterConfig.Nodes,
			Password:     property.Password,
			MaxRedirects: property.ClusterConfig.MaxRedirects,
			PoolSize:     property.PoolConfig.Size,
			MinIdleConns: property.PoolConfig.MinIdle,
		})

		redisClient = &clusterClient{
			Cmdable: client,
			client:  client,
		}
	}

	if r, err := redisClient.Ping().Result(); err != nil {
		slago.Logger().Fatal().Err(err).Msg("fail to connect redis")
	} else {
		slago.Logger().Info().Msgf("ready to connect redis: %v", r)
	}

	return redisClient
}

func (c *singleClient) Close() error {
	return c.client.Close()
}

func (c *clusterClient) Close() error {
	return c.client.Close()
}
