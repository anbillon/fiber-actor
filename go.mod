module gitlab.com/anbillon/fiber-actor

go 1.13

require (
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/robfig/cron/v3 v3.0.0
	gitlab.com/anbillon/fiber v0.0.5
	gitlab.com/anbillon/slago v0.3.0
)
