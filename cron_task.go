// Copyright (c) 2019 Anbillon Team (anbillonteam@gmail.com).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package actor

import (
	"github.com/robfig/cron/v3"
	"gitlab.com/anbillon/fiber"
	"gitlab.com/anbillon/slago"
)

type cronTask struct {
	cron       *cron.Cron
	schedulers []Scheduler
}

func init() {
	fiber.WireWithOption(newCronTask, fiber.Default(make([]Scheduler, 0)))
}

//newCronTask creates a new instance of cronTask.
func newCronTask(schedures []Scheduler) *cronTask {
	return &cronTask{
		cron:       cron.New(),
		schedulers: schedures,
	}
}

func (t *cronTask) OnAppStart() {
	if len(t.schedulers) == 0 {
		slago.Logger().Info().Msg("no schedulers found, skipping")
		return
	}

	for _, s := range t.schedulers {
		if _, err := t.cron.AddFunc(s.Cron(), s.Schedule); err != nil {
			slago.Logger().Fatal().Err(err).Msg("fail to reolve scheduler")
		}
	}

	slago.Logger().Info().Msg("start all cron tasks")
	t.cron.Start()
}
